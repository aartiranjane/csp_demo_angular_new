import {Injectable} from '@angular/core';

@Injectable()
export class UriService {
  getResourceServerUri() {
    return 'http://localhost:8085';
  }

}
