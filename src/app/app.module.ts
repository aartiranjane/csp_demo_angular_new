import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {LoginComponent} from './login/login.component';
import {HomeComponent} from './home/home.component';
import {PmComponent} from './pm/pm.component';
import {PmService} from './pm/pm.service';
import {UriService} from '../uri.service';
import {HttpClientModule} from '@angular/common/http';
import { LogicalConsumptionComponent } from './pm/logical-consumption/logical-consumption.component';
import { MpsComponent } from './pm/mps/mps.component';
import { GrossConsumptionComponent } from './pm/gross-consumption/gross-consumption.component';
import { StocksComponent } from './pm/stocks/stocks.component';
import { SupplyComponent } from './pm/supply/supply.component';
import { MouldSaturationComponent } from './pm/mould-saturation/mould-saturation.component';
import {RmComponent} from './rm/rm.component';
import {MastersComponent} from './masters/masters.component';
import {RscDtItemCodeComponent} from './masters/rsc-dt-item-code/rsc-dt-item-code.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatCardModule, MatFormFieldModule, MatTableModule} from '@angular/material';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { RSCDTItemCategoryComponent } from './masters/rsc-dt-item-category/rsc-dt-item-category.component';
import { RscDtCoverDaysComponent } from './masters/rsc-dt-cover-days/rsc-dt-cover-days.component';
import { RscDtUomComponent } from './masters/rsc-dt-uom/rsc-dt-uom.component';
import { RscDtDivisionComponent } from './masters/rsc-dt-division/rsc-dt-division.component';
import { RscDtSafetyStockComponent } from './masters/rsc-dt-safety-stock/rsc-dt-safety-stock.component';
import { RscDtItemClassComponent } from './masters/rsc-dt-item-class/rsc-dt-item-class.component';
import { RscDtTechnicalSeriesComponent } from './masters/rsc-dt-technical-series/rsc-dt-technical-series.component';
import { RscDtMoqComponent } from './masters/rsc-dt-moq/rsc-dt-moq.component';
import { RscDtTransportModeComponent } from './masters/rsc-dt-transport-mode/rsc-dt-transport-mode.component';
import { RscDtTransportTypeComponent } from './masters/rsc-dt-transport-type/rsc-dt-transport-type.component';
import { RscDtMouldComponent } from './masters/rsc-dt-mould/rsc-dt-mould.component';
import { RscDtSupplierComponent } from './masters/rsc-dt-supplier/rsc-dt-supplier.component';
import { RscDtProductionTypeComponent } from './masters/rsc-dt-production-type/rsc-dt-production-type.component';
import { RscDtLinesComponent } from './masters/rsc-dt-lines/rsc-dt-lines.component';
import { RscDtStockTypeComponent } from './masters/rsc-dt-stock-type/rsc-dt-stock-type.component';
import { RscDtSailingDaysComponent } from './masters/rsc-dt-sailing-days/rsc-dt-sailing-days.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    PmComponent,
    RmComponent,
    LogicalConsumptionComponent,
    MpsComponent,
    GrossConsumptionComponent,
    StocksComponent,
    SupplyComponent,
    MouldSaturationComponent,
    MastersComponent,
    RscDtItemCodeComponent,
    RSCDTItemCategoryComponent,
    RscDtCoverDaysComponent,
    RscDtUomComponent,
    RscDtDivisionComponent,
    RscDtSafetyStockComponent,
    RscDtItemClassComponent,
    RscDtTechnicalSeriesComponent,
    RscDtMoqComponent,
    RscDtTransportModeComponent,
    RscDtTransportTypeComponent,
    RscDtMouldComponent,
    RscDtSupplierComponent,
    RscDtProductionTypeComponent,
    RscDtLinesComponent,
    RscDtStockTypeComponent,
    RscDtSailingDaysComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatCardModule,
    MatFormFieldModule,
    MatTableModule,
    FormsModule,
    ReactiveFormsModule

  ],
  providers: [PmService, UriService],
  exports: [MatCardModule, MatFormFieldModule],
  bootstrap: [AppComponent]
})
export class AppModule {
}
