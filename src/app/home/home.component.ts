import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-main-page',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private router: Router) {
  }

  ngOnInit() {
  }

  pmPage() {
    this.router.navigate(['/pm']);
  }

  rmPage() {
    this.router.navigate(['/bulkAndRm']);
  }

  navigateToMaster() {
    this.router.navigate(['/masters']);
  }

}
