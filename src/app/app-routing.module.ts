import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {LoginComponent} from './login/login.component';
import {HomeComponent} from './home/home.component';
import {PmComponent} from './pm/pm.component';
import {RmComponent} from './rm/rm.component';
import {MastersComponent} from './masters/masters.component';
import {RscDtItemCodeComponent} from './masters/rsc-dt-item-code/rsc-dt-item-code.component';
import {RSCDTItemCategoryComponent} from './masters/rsc-dt-item-category/rsc-dt-item-category.component';
import {RscDtCoverDaysComponent} from './masters/rsc-dt-cover-days/rsc-dt-cover-days.component';
import {RscDtUomComponent} from './masters/rsc-dt-uom/rsc-dt-uom.component';
import {RscDtDivisionComponent} from './masters/rsc-dt-division/rsc-dt-division.component';
import {RscDtSafetyStockComponent} from './masters/rsc-dt-safety-stock/rsc-dt-safety-stock.component';
import {RscDtItemClassComponent} from './masters/rsc-dt-item-class/rsc-dt-item-class.component';
import {RscDtTechnicalSeriesComponent} from './masters/rsc-dt-technical-series/rsc-dt-technical-series.component';
import {RscDtMoqComponent} from './masters/rsc-dt-moq/rsc-dt-moq.component';
import {RscDtTransportModeComponent} from './masters/rsc-dt-transport-mode/rsc-dt-transport-mode.component';
import {RscDtTransportTypeComponent} from './masters/rsc-dt-transport-type/rsc-dt-transport-type.component';
import {RscDtMouldComponent} from './masters/rsc-dt-mould/rsc-dt-mould.component';
import {RscDtSupplierComponent} from './masters/rsc-dt-supplier/rsc-dt-supplier.component';
import {RscDtProductionTypeComponent} from './masters/rsc-dt-production-type/rsc-dt-production-type.component';
import {RscDtLinesComponent} from './masters/rsc-dt-lines/rsc-dt-lines.component';
import {RscDtStockTypeComponent} from './masters/rsc-dt-stock-type/rsc-dt-stock-type.component';
import {RscDtSailingDaysComponent} from './masters/rsc-dt-sailing-days/rsc-dt-sailing-days.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';

const routes: Routes = [
  {path: '', redirectTo: '/login', pathMatch: 'full'},
  {path: 'login', component: LoginComponent},
  {path: 'home', component: HomeComponent},
  {path: 'pm', component: PmComponent},
  {path: 'bulkAndRm', component: RmComponent},
  {
    path: 'masters', component: MastersComponent, children: [
      {path: 'rscDtItemCode', component: RscDtItemCodeComponent},
      {path: 'rscDtItemCategory', component: RSCDTItemCategoryComponent},
      {path: 'rscDtCoverDays', component: RscDtCoverDaysComponent},
      {path: 'rscDtUom', component: RscDtUomComponent},
      {path: 'rscDtDivision', component: RscDtDivisionComponent},
      {path: 'rscDtSafetyStock', component: RscDtSafetyStockComponent},
      {path: 'rscDtItemClass', component: RscDtItemClassComponent},
      {path: 'rscDtTechnicalSeries', component: RscDtTechnicalSeriesComponent},
      {path: 'rscDtMoq', component: RscDtMoqComponent},
      {path: 'rscDtTransportMode', component: RscDtTransportModeComponent},
      {path: 'rscDtTransportType', component: RscDtTransportTypeComponent},
      {path: 'rscDtMould', component: RscDtMouldComponent},
      {path: 'rscDtSupplier', component: RscDtSupplierComponent},
      {path: 'rscDtProductionType', component: RscDtProductionTypeComponent},
      {path: 'rscDtLines', component: RscDtLinesComponent},
      {path: 'rscDtStockType', component: RscDtStockTypeComponent},
      {path: 'rscDtSailingDays', component: RscDtSailingDaysComponent}
    ]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes), MatFormFieldModule, MatInputModule],
  exports: [RouterModule, MatFormFieldModule, MatInputModule]
})
export class AppRoutingModule {
}
