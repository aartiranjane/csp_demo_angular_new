import { Component, OnInit } from '@angular/core';
import {MatTableDataSource} from '@angular/material';
import {RscDtProductionTypeService} from './rsc-dt-production-type.service';

@Component({
  selector: 'app-rsc-dt-production-type',
  templateUrl: './rsc-dt-production-type.component.html',
  styleUrls: ['./rsc-dt-production-type.component.css']
})
export class RscDtProductionTypeComponent implements OnInit {
  dataSource: MatTableDataSource<any>;
  rscDtProductionTypeList = [];
  displayedColumns: string[] = ['name'];

  constructor(private rscDtProductionTypeService: RscDtProductionTypeService) {
  }

  ngOnInit() {
    this.rscDtProductionTypeService.getRscDtProductionTypeList().subscribe((rscDtProductionTypeResponse: any) => {
        this.rscDtProductionTypeList = rscDtProductionTypeResponse;
        if (rscDtProductionTypeResponse != null) {
          const rscDtProductionTypeView = [];
          rscDtProductionTypeResponse.forEach((response) => {
              {
                rscDtProductionTypeView.push({
                  name: response.name
                });
                this.dataSource = new MatTableDataSource(rscDtProductionTypeView);
              }

            }
          );
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

}
