import { Component, OnInit } from '@angular/core';
import {MatTableDataSource} from '@angular/material';
import {RscDtTransportTypeService} from './rsc-dt-transport-type.service';

@Component({
  selector: 'app-rsc-dt-transport-type',
  templateUrl: './rsc-dt-transport-type.component.html',
  styleUrls: ['./rsc-dt-transport-type.component.css']
})
export class RscDtTransportTypeComponent implements OnInit {
  dataSource: MatTableDataSource<any>;
  rscDtTransportTypeList = [];
  displayedColumns: string[] = ['alias_name', 'full_name'];

  constructor(private rscDtTransportTypeService: RscDtTransportTypeService) {
  }

  ngOnInit() {
    this.rscDtTransportTypeService.getRscDtTransportTypeList().subscribe((rscDtTransportTypeResponse: any) => {
        this.rscDtTransportTypeList = rscDtTransportTypeResponse;
        if (rscDtTransportTypeResponse != null) {
          const rscDtTransportTypeView = [];
          rscDtTransportTypeResponse.forEach((response) => {
              {
                rscDtTransportTypeView.push({
                  alias_name: response.aliasName,
                  full_name: response.fullName
                });
                this.dataSource = new MatTableDataSource(rscDtTransportTypeView);
              }

            }
          );
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

}
