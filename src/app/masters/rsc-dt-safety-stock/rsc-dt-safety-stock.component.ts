import { Component, OnInit } from '@angular/core';
import {MatTableDataSource} from '@angular/material';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {RscDtSafetyStockService} from './rsc-dt-safety-stock.service';

@Component({
  selector: 'app-rsc-dt-safety-stock',
  templateUrl: './rsc-dt-safety-stock.component.html',
  styleUrls: ['./rsc-dt-safety-stock.component.css']
})

export class RscDtSafetyStockComponent implements OnInit {
  dataSource: MatTableDataSource<any>;
  rscDtSafetyStockList = [];
  displayedColumns: string[] = ['stock_value'];

  constructor(private rscDtSafetyStockService: RscDtSafetyStockService) { }

  ngOnInit() {
    this.rscDtSafetyStockService.getRscDtSafetyStockList().subscribe((rscDtSafetyStockResponse: any) => {
        this.rscDtSafetyStockList = rscDtSafetyStockResponse;
        if (rscDtSafetyStockResponse != null) {
          const rscDtSafetyStockView = [];
          rscDtSafetyStockResponse.forEach((response) => {
              {
                rscDtSafetyStockView.push({
                  stock_value: response.stockValue
                });
                this.dataSource = new MatTableDataSource(rscDtSafetyStockView);
              }

            }
          );
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

}
