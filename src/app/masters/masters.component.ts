import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-masters',
  templateUrl: './masters.component.html',
  styleUrls: ['./masters.component.css']
})
export class MastersComponent implements OnInit {
  mastersList = [
    ['Rsc DT ItemCode', 'rscDtItemCode'],
    ['Rsc DT ItemCategory', 'rscDtItemCategory'],
    ['Rsc DT CoverDays', 'rscDtCoverDays'],
    ['Rsc DT Uom', 'rscDtUom'],
    ['Rsc DT Division', 'rscDtDivision'],
    ['Rsc DT Safety Stock', 'rscDtSafetyStock'],
    ['Rsc DT Item Class', 'rscDtItemClass'],
    ['Rsc DT Technical Series', 'rscDtTechnicalSeries'],
    ['Rsc DT Moq', 'rscDtMoq'],
    ['Rsc DT Transport Mode', 'rscDtTransportMode'],
    ['Rsc DT Transport Type', 'rscDtTransportType'],
    ['Rsc DT Mould', 'rscDtMould'],
    ['Rsc DT Supplier', 'rscDtSupplier'],
    ['Rsc DT Production Type', 'rscDtProductionType'],
    ['Rsc DT Lines', 'rscDtLines'],
    ['Rsc DT Stock Type', 'rscDtStockType'],
    ['Rsc DT Sailing Days', 'rscDtSailingDays']
  ];
  currentMasterName: string;

  constructor(private router: Router) {
  }

  ngOnInit() {
  }

  loadMasterData(masterName, masterUrl) {
    this.currentMasterName = masterName;
    this.router.navigate(['/masters/' + masterUrl]);
  }
}
