import { Component, OnInit } from '@angular/core';
import {MatTableDataSource} from '@angular/material';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {RscDtDivisionService} from './rsc-dt-division.service';

@Component({
  selector: 'app-rsc-dt-division',
  templateUrl: './rsc-dt-division.component.html',
  styleUrls: ['./rsc-dt-division.component.css']
})
export class RscDtDivisionComponent implements OnInit {
  dataSource: MatTableDataSource<any>;
  rscDtDivisionList = [];
  displayedColumns: string[] = ['alias_name', 'full_name'];

  constructor(private rscDtDivisionService: RscDtDivisionService) {
  }

  ngOnInit() {
    this.rscDtDivisionService.getRscDtDivisionList().subscribe((rscDtDivisionResponse: any) => {
        this.rscDtDivisionList = rscDtDivisionResponse;
        if (rscDtDivisionResponse != null) {
          const rscDtDivisionView = [];
          rscDtDivisionResponse.forEach((response) => {
              {
                rscDtDivisionView.push({
                  alias_name: response.aliasName,
                  full_name: response.fullName
                });
                this.dataSource = new MatTableDataSource(rscDtDivisionView);
              }

            }
          );
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

}
