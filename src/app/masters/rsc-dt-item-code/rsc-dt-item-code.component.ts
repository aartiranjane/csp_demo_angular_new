import {Component, OnInit} from '@angular/core';
import {MatTableDataSource} from '@angular/material';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {RscDtItemCodeService} from './rsc-dt-item-code.service';

@Component({
  selector: 'app-rsc-dt-item-code',
  templateUrl: './rsc-dt-item-code.component.html',
  styleUrls: ['./rsc-dt-item-code.component.css']
})
export class RscDtItemCodeComponent implements OnInit {
  dataSource: MatTableDataSource<any>;
  rscDtItemCodeList = [];
  displayedColumns: string[] = ['code', 'description'];

  constructor(private rscDtItemCodeService: RscDtItemCodeService) {
  }

  ngOnInit() {
    this.rscDtItemCodeService.getRscDtItemCodeList().subscribe((rscDtItemCodeResponse: any) => {
        this.rscDtItemCodeList = rscDtItemCodeResponse;
        if (rscDtItemCodeResponse != null) {
          const rscDtItemCodeView = [];
          rscDtItemCodeResponse.forEach((response) => {
              {
                rscDtItemCodeView.push({
                  code: response.code,
                  description: response.description
                });
                this.dataSource = new MatTableDataSource(rscDtItemCodeView);
              }

            }
          );
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

}
