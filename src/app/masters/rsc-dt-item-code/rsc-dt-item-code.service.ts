import {Injectable} from '@angular/core';
import {Subject, throwError} from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {catchError, map} from 'rxjs/operators';
import {UriService} from '../../../uri.service';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class RscDtItemCodeService {
  endpoint = this.uriService.getResourceServerUri();

  constructor(private http: HttpClient,
              private uriService: UriService) {
  }

  getRscDtItemCodeList() {
    return this.http.get(this.endpoint + '/api/rsc-dt-item-codes', httpOptions).pipe(map((response: Response) => response),
      catchError(this.errorHandler));
  }

  errorHandler(error: Response) {
    return throwError(error || 'Server Error');
  }
}
