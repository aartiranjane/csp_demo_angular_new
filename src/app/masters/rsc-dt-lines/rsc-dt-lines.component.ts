import { Component, OnInit } from '@angular/core';
import {MatTableDataSource} from '@angular/material';
import {RscDtLinesService} from './rsc-dt-lines.service';

@Component({
  selector: 'app-rsc-dt-lines',
  templateUrl: './rsc-dt-lines.component.html',
  styleUrls: ['./rsc-dt-lines.component.css']
})
export class RscDtLinesComponent implements OnInit {
  dataSource: MatTableDataSource<any>;
  rscDtLinesList = [];
  displayedColumns: string[] = ['name'];

  constructor(private rscDtLinesService: RscDtLinesService) {
  }

  ngOnInit() {
    this.rscDtLinesService.getRscDtLinesList().subscribe((rscDtLinesResponse: any) => {
        this.rscDtLinesList = rscDtLinesResponse;
        if (rscDtLinesResponse != null) {
          const rscDtLinesView = [];
          rscDtLinesResponse.forEach((response) => {
              {
                rscDtLinesView.push({
                  name: response.name
                });
                this.dataSource = new MatTableDataSource(rscDtLinesView);
              }

            }
          );
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

}
