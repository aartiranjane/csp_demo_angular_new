import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {UriService} from '../../../uri.service';
import {catchError, map} from 'rxjs/operators';
import {throwError} from 'rxjs';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})

export class RscDtTechnicalSeriesService {
  endpoint = this.uriService.getResourceServerUri();

  constructor(private http: HttpClient,
              private uriService: UriService) {
  }

  getRscDtTechnicalSeriesList() {
    return this.http.get(this.endpoint + '/api/rsc-dt-technical-series', httpOptions).pipe(map((response: Response) => response),
      catchError(this.errorHandler));
  }

  errorHandler(error: Response) {
    return throwError(error || 'Server Error');
  }
}
