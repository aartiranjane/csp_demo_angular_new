import { Component, OnInit } from '@angular/core';
import {MatTableDataSource} from '@angular/material';
import {RscDtMouldService} from './rsc-dt-mould.service';

@Component({
  selector: 'app-rsc-dt-mould',
  templateUrl: './rsc-dt-mould.component.html',
  styleUrls: ['./rsc-dt-mould.component.css']
})
export class RscDtMouldComponent implements OnInit {
  dataSource: MatTableDataSource<any>;
  rscDtMouldList = [];
  displayedColumns: string[] = ['mould', 'child_mould', 'no_of_cavities', 'per_cavity', 'total_capacity', 'active'];

  constructor(private rscDtMouldService: RscDtMouldService) {
  }

  ngOnInit() {
    this.rscDtMouldService.getRscDtMouldList().subscribe((rscDtMouldResponse: any) => {
        this.rscDtMouldList = rscDtMouldResponse;
        if (rscDtMouldResponse != null) {
          const rscDtMouldView = [];
          rscDtMouldResponse.forEach((response) => {
              {
                rscDtMouldView.push({
                  mould: response.mould,
                  child_mould: response.childMould,
                  no_of_cavities: response.noOfCavities,
                  per_cavity: response.perCavity,
                  total_capacity: response.totalCapacity,
                  active: response.active,
                });
                this.dataSource = new MatTableDataSource(rscDtMouldView);
              }

            }
          );
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

}
