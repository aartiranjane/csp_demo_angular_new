import { Component, OnInit } from '@angular/core';
import {MatTableDataSource} from '@angular/material';
import {RscDtMoqService} from './rsc-dt-moq.service';

@Component({
  selector: 'app-rsc-dt-moq',
  templateUrl: './rsc-dt-moq.component.html',
  styleUrls: ['./rsc-dt-moq.component.css']
})
export class RscDtMoqComponent implements OnInit {
  dataSource: MatTableDataSource<any>;
  rscDtMoqList = [];
  displayedColumns: string[] = ['moq_value'];

  constructor(private rscDtMoqService: RscDtMoqService) { }

  ngOnInit() {
    this.rscDtMoqService.getRscDtMoqList().subscribe((rscDtMoqResponse: any) => {
        this.rscDtMoqList = rscDtMoqResponse;
        if (rscDtMoqResponse != null) {
          const rscDtMoqView = [];
          rscDtMoqResponse.forEach((response) => {
              {
                rscDtMoqView.push({
                  moq_value: response.moqValue
                });
                this.dataSource = new MatTableDataSource(rscDtMoqView);
              }

            }
          );
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }

}
