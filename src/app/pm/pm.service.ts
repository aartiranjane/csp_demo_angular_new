import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {UriService} from '../../uri.service';
import {catchError, map} from 'rxjs/operators';
import {throwError} from 'rxjs';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class PmService {

  endpoint = this.uriService.getResourceServerUri();

  constructor(private http: HttpClient,
              private uriService: UriService) {
  }

  getLogicalConsumptionData() {
    return this.http.get(this.endpoint + '/api/logical-consumption/pm', httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getGrossConsumptionData() {
    return this.http.get(this.endpoint + '/api/gross-consumption/pm', httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getMpsData() {
    return this.http.get(this.endpoint + '/api/mps/production-plan', httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  errorHandler(error: Response) {
    return throwError(error || 'Server Error');
  }
}
