import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {PmService} from '../pm.service';

@Component({
  selector: 'app-mps',
  templateUrl: './mps.component.html',
  styleUrls: ['./mps.component.css']
})
export class MpsComponent implements OnInit {
  mpsDataList = [];
  mpsDate: any;
  monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
    'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
  formattedDateList: any = [];
  constructor(private router: Router, private pmService: PmService) { }

  ngOnInit() {
    this.pmService.getLogicalConsumptionData().subscribe((mpsDateResponse: any) => {
      console.log(mpsDateResponse);
   /*   this.logicalConsumptionDataList = logicalConsumptionResponse.pmLogicalConsumptionMainDTOList;*/
      this.mpsDate = new Date(mpsDateResponse.mpsDate);
      this.getFormattedDateList();
    });

    this.getMrpDataList();
  }
  getFormattedDateList() {
    // const dates = this.mpsDate.getDate();
    let month: number = this.mpsDate.getMonth();
    let year = this.mpsDate.getFullYear();
    for (let i = 0; i < 12; i++) {
      if (month === 12) {
        month = 0;
        year = year + 1;
      }
      this.formattedDateList.push(this.monthNames[month] + '\'' + year % 100);
      month = month + 1;
    }
  }
  getMrpDataList() {
    this.pmService.getMpsData().subscribe((mpsDataResponse: any) => {
      console.log(mpsDataResponse);
      this.mpsDataList = mpsDataResponse;
    });
  }

}
