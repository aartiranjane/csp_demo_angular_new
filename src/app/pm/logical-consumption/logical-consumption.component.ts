import { Component, OnInit } from '@angular/core';
import {PmService} from '../pm.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-logical-consumption',
  templateUrl: './logical-consumption.component.html',
  styleUrls: ['./logical-consumption.component.css']
})
export class LogicalConsumptionComponent implements OnInit {

  selectedName: any;
  logicalConsumptionDataList: any = [];
  mpsDate: any;

  monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
    'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'
  ];
  formattedDateList: any = [];

  constructor(private router: Router, private pmService: PmService) { }

  ngOnInit(): void {
    this.pmService.getLogicalConsumptionData().subscribe((logicalConsumptionResponse: any) => {
      console.log(logicalConsumptionResponse);
      this.logicalConsumptionDataList = logicalConsumptionResponse.pmLogicalConsumptionMainDTOList;
      this.mpsDate = new Date(logicalConsumptionResponse.mpsDate);
      this.getFormattedDateList();
    });


  }
  getFormattedDateList() {
    // const dates = this.mpsDate.getDate();
    let month: number = this.mpsDate.getMonth();
    let year = this.mpsDate.getFullYear();
    for (let i = 0; i < 12; i++) {
      if (month === 12) {
        month = 0;
        year = year + 1;
      }
      this.formattedDateList.push(this.monthNames[month] + '\'' + year % 100);
      month = month + 1;
    }
  }

  public highlightRow(emp: any) {

    console.log(emp);
    console.log(emp);
    this.selectedName = emp;
  }

}
