import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-rm',
  templateUrl: './rm.component.html',
  styleUrls: ['./rm.component.css']
})
export class RmComponent implements OnInit {


  blinkLogicalTab ={
    'pulse2' : true
  }
  blinkGrossTab ={
    'pulse2' : false
  }
  isConsumption1 = 'active enabled';
  isConsumption2 = '';
  isConsumption3 = '';
  showTab = 1;



  constructor( private router: Router) {

  }
  /* constructor(private router: Router) { }*/

  ngOnInit(): void {
  }


  executeBtn(){
    this.isConsumption2 = 'active enabled';
    this.isConsumption1 = '';
    this.isConsumption3 = '';
    this.showTab = 2
  }
  tab(index:any){
    if(index == 1){
      this.isConsumption1 = 'active enabled';
      this.isConsumption2 = '';
      this.isConsumption3 = '';
      this.showTab = 1;

    }
    else if(index ==  2){
      this.isConsumption2 = 'active enabled';
      this.isConsumption1 = '';
      this.isConsumption3 = '';
      this.blinkLogicalTab.pulse2 = false;
      this.blinkGrossTab.pulse2 = true;
      this.showTab = 2;

    }
    else if(index ==  3){
      this.isConsumption3 = 'active enabled';
      this.isConsumption1 = '';
      this.isConsumption2 = '';
      this.blinkGrossTab.pulse2 = false;
      this.showTab = 3;

    }

  }

}
